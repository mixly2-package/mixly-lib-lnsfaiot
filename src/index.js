import * as Blockly from 'blockly/core';
import './language/loader';
import * as generators from './generators/LNSF_AIoT';
import * as blocks from './blocks/LNSF_AIoT';
import './css/lib.css';

// 载入图形化模块外观定义文件
Object.assign(Blockly.Blocks, blocks);

// 载入图形化模块代码生成定义文件
if (Blockly.Python.forBlock) {
    Object.assign(Blockly.Python.forBlock, generators);
} else {
    Object.assign(Blockly.Python, generators);
}