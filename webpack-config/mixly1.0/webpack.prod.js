const path = require('path');
const common = require('./webpack.common');
const { merge } = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const libInfo = `<!--
  type="company"
  block="block/lnsfaiot.js"
  generator="generator/lnsfaiot.js"
  lib="lnsfaiot"
  media="media/lnsfaiot"
  language="language/lnsfaiot"
-->
<script type="text/javascript" src="../../blocks/company/lnsfaiot.js"></script>`;


module.exports = merge(common, {
    mode: 'production',
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                extractComments: false,
            }),
            new HtmlWebpackPlugin({
                inject: false,
                template: path.resolve(process.cwd(), 'src/template.xml'),
                filename: 'lnsfaiot.xml',
                minify: false,
                publicPath: `../../`,
                excludeChunks: [
                    'language/lnsfaiot/zh-hans',
                    'language/lnsfaiot/zh-hant',
                    'language/lnsfaiot/en',
                    'block/lnsfaiot',
                    'generator/lnsfaiot'
                ],
                templateParameters: (compilation, assets, assetTags, options) => {
                    assetTags.headTags.push(libInfo);
                    return {
                        compilation,
                        webpackConfig: compilation.options,
                        htmlWebpackPlugin: {
                            tags: assetTags,
                            files: assets,
                            options
                        }
                    };
                }
            })
        ]
    }
});
